ActiveAdmin.register Season do
    index do 
        column :serial
        column :number
        default_actions
    end
    form :html => { :enctype => "multipart/form-data" } do |f|
        f.inputs "Season" do 
            f.inputs :serial, :number
        end 

        f.inputs "Episodes" do 
            f.has_many :episodes do |episode|
                episode.input :number
                episode.input :content, :as => :file
            end
        end
        f.actions
    end
end
