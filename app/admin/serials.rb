ActiveAdmin.register Serial do
    filter :name
    index do 
        column :name
        column :description
        column :category
        default_actions
    end

    form :html => { :enctype => "multipart/form-data"} do |f|  
        f.inputs "Details" do
            f.input :category, :as => :select, :collection => Category.all
            f.input :name 
            f.input :description
            f.input :cover, :as => :file 
        end
        f.buttons
    end
end
