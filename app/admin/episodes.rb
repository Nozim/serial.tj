# encoding:  utf-8
ActiveAdmin.register Episode do
    index do 
        column :number
        column "Сериал" do |episode|
            episode.season.serial.name 
        end
        column "Сезон" do |episode|
            episode.season.number
        end
        default_actions 
    end
    

    form do |f| 
        f.inputs "Details" do 
            f.input :season, :collection => Season.all.map { |season| [season.number, season.id]  }
            f.input :number
            f.input :content, :as => :file 
        end
        f.buttons
    end 
end
