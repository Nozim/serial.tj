function EpisodesController($scope)
{
    
    $scope.video_url="/episodes/";
    $scope.video = document.getElementById('video_player');
    $scope.volume_level = 0.3
    $scope.video.addEventListener('ended',function()
    {
        $scope.video.src = $scope.video_url+$scope.current_episode+1
    })

    $scope.init = function()
    { 
       $scope.video.volume = $scope.volume_level 
    }

    $scope.play_episode =  function(episode)
    {
       $scope.video.volume = $scope.volume_level 
       $scope.video.src = $scope.video_url + episode
       $scope.video.play()
       $scope.current_episode = episode
    }

}
