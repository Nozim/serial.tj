class EpisodesController < ApplicationController 
    def show 
       @episode = Episode.find(params[:id]) 
       send_file @episode.content.path
    end
end
