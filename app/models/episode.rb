class Episode < ActiveRecord::Base
  belongs_to :season
  attr_accessible :number, :season_id, :content
  has_attached_file :content 
  validates_uniqueness_of :number, :scope => [:season_id]
  validates_presence_of :number, :content
end
