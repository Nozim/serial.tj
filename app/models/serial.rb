class Serial < ActiveRecord::Base
  has_many :seasons
  belongs_to :category
  attr_accessible :description, :name, :cover, :category_id
  has_attached_file :cover
end
