class Category < ActiveRecord::Base
  has_many :serials
  attr_accessible :name, :serial_id

end
