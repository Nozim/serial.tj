class Season < ActiveRecord::Base
  belongs_to :serial
  attr_accessible :number, :serial_id, :episodes_attributes
  has_many :episodes, :dependent => :destroy 
  validates_presence_of :number, :serial_id
  validates_associated :episodes
  validates_uniqueness_of :number, :scope => [:serial_id]

  accepts_nested_attributes_for :episodes, :reject_if => :all_blank 
  
end
