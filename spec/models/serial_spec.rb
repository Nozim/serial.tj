require 'spec_helper'

describe Serial do 
    it { should belong_to(:category) }
    it { should have_many(:seasons) }
end

