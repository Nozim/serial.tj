SerialTj::Application.routes.draw do
  ActiveAdmin.routes(self)

  devise_for :admin_users, ActiveAdmin::Devise.config

  get 'home/index'
  root :to => "home#index"
  resources :categories, :only => :show 
  resources :serials, :only => :show 
  resources :episodes, :only => :show 
end
