class AddAttachmentCoverToSerials < ActiveRecord::Migration
  def self.up
    change_table :serials do |t|
      t.attachment :cover
    end
  end

  def self.down
    drop_attached_file :serials, :cover
  end
end
