class AddCategoryIdToSerial < ActiveRecord::Migration
  def change
    add_column :serials, :category_id, :integer
  end
end
