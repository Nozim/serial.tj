class AddSerialIdToSeason < ActiveRecord::Migration
  def change
    add_column :seasons, :serial_id, :integer
    add_column :episodes, :season_id, :integer
  end
end
