class AddAttachmentContentToEpisodes < ActiveRecord::Migration
  def self.up
    change_table :episodes do |t|
      t.attachment :content
    end
  end

  def self.down
    drop_attached_file :episodes, :content
  end
end
